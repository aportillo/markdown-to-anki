package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"time"
)

type AnkiResponseGetDecks struct {
	Result *[]string `json:"result"`
	Error  *string   `json:"error"`
}

type AnkiResultGetDecks struct {
	Data *[]string
}

type AnkiResponseCreateItem struct {
	Result *int64  `json:"result"`
	Error  *string `json:"error"`
}

type AnkiResponseCreateItems struct {
	Result *[]*int64 `json:"result"`
	Error  *string   `json:"error"`
}

type AnkiFields struct {
	Front string `json:"Front"`
	Back  string `json:"Back"`
}

type AnkiNote struct {
	DeckName  string      `json:"deckName"`
	ModelName string      `json:"modelName"`
	Fields    AnkiFields  `json:"fields"`
	Options   AnkiOptions `json:"options"`
}

type AnkiOptions struct {
	DuplicateScope string `json:"duplicateScope"`
}

type AnkiParamsNote struct {
	Note AnkiNote `json:"note"`
}

type AnkiParamsNotes struct {
	Notes []AnkiNote `json:"notes"`
}

type AnkiParamsDeck struct {
	Deck string `json:"deck"`
}

type AnkiRequestSimple struct {
	Action  string `json:"action"`
	Version int    `json:"version"`
}

type AnkiRequestNotes struct {
	Action  string          `json:"action"`
	Version int             `json:"version"`
	Params  AnkiParamsNotes `json:"params,omitempty"`
}

type AnkiRequestNote struct {
	Action  string         `json:"action"`
	Version int            `json:"version"`
	Params  AnkiParamsNote `json:"params,omitempty"`
}

type AnkiRequestDeck struct {
	Action  string         `json:"action"`
	Version int            `json:"version"`
	Params  AnkiParamsDeck `json:"params,omitempty"`
}

// Anki server occassionally cant keep up with requests so we need
// to retry. One retry is usually enough.
func postRequestWithRetries(requestBody []byte) ([]byte, error) {
	backOffSchedule := []time.Duration{
		500 * time.Millisecond,
		1 * time.Second,
		3 * time.Second,
	}

	var err error
	var responseBody []byte
	for _, backoff := range backOffSchedule {
		responseBody, err = postRequest(requestBody)
		if err == nil {
			break
		}
		time.Sleep(backoff)
	}

	if err != nil {
		return nil, err
	}

	return responseBody, nil
}

// Make POST request to Anki server. The action type is passed in the request body.
func postRequest(requestBody []byte) ([]byte, error) {
	payLoad := bytes.NewBuffer(requestBody)
	response, err := http.Post(ANKI_BASE_URL, "application/json", payLoad)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

// Batch note adding. When adding in batches, missing deck error is suppressed,
// we need to try adding a single note if we want to catch a missing deck and
// create it.
func addNotes(notes []InternalNote) (*[]*int64, error) {

	if len(notes) == 0 {
		return nil, nil
	}

	// pop off first element
	first, notes := notes[0], notes[1:]

	firstID, err := addNoteWithDeckCreation(first)
	if err != nil {
		return nil, err
	}
	if len(notes) == 0 {
		return nil, nil
	}

	// begin batch note creation
	var data AnkiResponseCreateItems
	payload := AnkiRequestNotes{
		Action:  "addNotes",
		Version: 6,
		Params: AnkiParamsNotes{
			Notes: []AnkiNote{},
		},
	}

	for _, note := range notes {
		payload.Params.Notes = append(payload.Params.Notes,
			AnkiNote{
				DeckName:  note.deck,
				ModelName: "Basic",
				Fields: AnkiFields{
					Front: note.front,
					Back:  note.back,
				},
			},
		)
	}

	body, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	response, err := postRequestWithRetries(body)
	if err != nil {
		return nil, err
	}

	json.Unmarshal(response, &data)
	if data.Error != nil {
		return nil, errors.New(*data.Error)
	}

	// prepend first ID to response
	newResult := []*int64{firstID}
	newResult = append(newResult, *data.Result...)

	return &newResult, nil
}

// Request to Anki server to add a single note.
func addNote(note InternalNote) (*int64, error) {
	var data AnkiResponseCreateItem
	payload := AnkiRequestNote{
		Action:  "addNote",
		Version: 6,
		Params: AnkiParamsNote{
			Note: AnkiNote{
				DeckName:  note.deck,
				ModelName: "Basic",
				Fields: AnkiFields{
					Front: note.front,
					Back:  note.back,
				},
				Options: AnkiOptions{
					DuplicateScope: "deck",
				},
			},
		},
	}
	body, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	response, err := postRequestWithRetries(body)
	if err != nil {
		return nil, err
	}

	json.Unmarshal(response, &data)
	if data.Error != nil {
		return nil, errors.New(*data.Error)
	}

	return data.Result, nil
}

// Request to Anki server to create a new deck.
func createDeck(deckName string) (*int64, error) {
	var data AnkiResponseCreateItem
	payload := AnkiRequestDeck{
		Action:  "createDeck",
		Version: 6,
		Params: AnkiParamsDeck{
			Deck: deckName,
		},
	}

	body, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	response, err := postRequestWithRetries(body)
	if err != nil {
		return nil, err
	}

	json.Unmarshal(response, &data)
	if data.Error != nil {
		return nil, errors.New(*data.Error)
	}

	return data.Result, nil
}

// Request to Anki server to get all existing deck names.
// Note: This isn't used anywhere, implemented for initial testing.
func getDeckNames() (*AnkiResultGetDecks, error) {
	var data AnkiResponseGetDecks
	payload := AnkiRequestSimple{
		Action:  "deckNames",
		Version: 6,
	}

	body, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	response, err := postRequestWithRetries(body)
	if err != nil {
		return nil, err
	}

	json.Unmarshal(response, &data)
	if data.Error != nil {
		return nil, errors.New(*data.Error)
	}

	return &AnkiResultGetDecks{Data: data.Result}, nil
}
