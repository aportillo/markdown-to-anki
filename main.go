// TODO: support checking for updates and deletions

package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/BurntSushi/toml"
)

type Config struct {
	MarkdownFile string
}

// 'Internal' differentiates between Anki and script
// representation of a 'note'
type InternalNote struct {
	front      string
	back       string
	deck       string
	id         *int64
	lineNumber int
}

// note: in Anki, a 'card' is the representation of the data in a 'note'
const ANKI_BASE_URL = "http://localhost:8765"
const ERROR_DECK_NOT_FOUND = "deck was not found"
const ERROR_DUPLICATE_NOTE = "cannot create note because it is a duplicate"

func main() {
	var currentDeck string
	var currentLineNumber int
	var noteCount int
	var notesToCreate []InternalNote
	var notesToCompare []InternalNote

	filePath := getFilePath()

	// setup logging, log to file and stdout
	logFile, err := os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening log file: %v\n", err)
	}
	defer logFile.Close()
	writer := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(writer)

	// load markdown file to process
	markdownFile, err := getFile(filePath)
	if err != nil {
		panic(err.Error())
	}
	defer markdownFile.Close()

	// scan file and group lines/notes to process
	scanner := bufio.NewScanner(markdownFile)
	for scanner.Scan() {
		currentLineNumber += 1
		line := scanner.Text()

		// we store the heading as the deckname
		if strings.HasPrefix(line, "#") {
			if currentDeck != "" {
				// process previous deck group, skip on first heading instance
				_, err := addNotes(notesToCreate)
				if err != nil {
					log.Printf("warn: error while batch creating notes: %s\n", err.Error())
				}
				log.Printf("processed %d lines for deck '%s'\n", noteCount, currentDeck)
			}
			currentDeck = updateDeckMarker(line)

			// reset counters
			noteCount = 0
			notesToCreate = nil
			notesToCompare = nil
		}

		// bullet point represents an anki note to save
		if strings.HasPrefix(line, "-") {
			note, err := parseNoteFromLine(line, currentDeck, currentLineNumber)
			if err != nil {
				log.Printf("warn: skipping line %d, %s\n", currentLineNumber, err.Error())
				continue
			} else {
				noteCount += 1
			}
			if note.id != nil {
				notesToCompare = append(notesToCompare, *note)
			} else {
				notesToCreate = append(notesToCreate, *note)
			}

		}

	}
	// handle last section in document
	_, err = addNotes(notesToCreate)
	if err != nil {
		log.Printf("warn: error while batch creating notes: %s\n", err.Error())
	}
	log.Printf("processed %d lines for deck '%s'\n", noteCount, currentDeck)
}

// Load markdown file path from config file.
func getFilePath() string {
	cwd, err := os.Getwd()
	if err != nil {
		log.Panic(err.Error())
	}

	var config Config
	_, err = toml.DecodeFile(fmt.Sprintf("%s/config.toml", cwd), &config)
	if err != nil {
		log.Panic(err.Error())
	}

	return config.MarkdownFile
}

// Send request to Anki server to create a new note. If we receive a non-existent
// deck error, we create the deck.
func addNoteWithDeckCreation(note InternalNote) (*int64, error) {
	if note.id != nil {
		// NOTE: writing note ids to markdown isn't implemented yet,
		// this would mean card already exists
	}
	noteID, err := addNote(note)
	if err != nil {
		if strings.HasPrefix(err.Error(), ERROR_DECK_NOT_FOUND) {
			// if deck not found, create the deck
			_, err := createDeck(note.deck)
			if err != nil {
				return nil, err
			}
			log.Printf("created new deck '%s'\n", note.deck)
			noteID, err = addNote(note)
			if err != nil {
				log.Printf("failed to add first note: %v\n", err.Error())
				return nil, err
			}
		} else if err.Error() == ERROR_DUPLICATE_NOTE {
			// card already exists, we can skip
		} else {
			return nil, err
		}
	}
	return noteID, nil
}

// Gets file object for markdown file
func getFile(path string) (*os.File, error) {
	file, err := os.Open(path)
	return file, err
}

// Pulls out deckname from header in file
func updateDeckMarker(line string) string {
	return strings.ReplaceAll(line[2:], " ", "")
}

// Parses bullet point and returns Card struct
func parseNoteFromLine(line string, deckName string, lineNumber int) (*InternalNote, error) {
	var startIndex int = 2
	var noteID *int64

	// case where a note has already been created and we've stored an anki note ID on our end
	// NOTE: this isn't implemented yet, shouldn't occur with normal usage
	if strings.HasPrefix(line[2:], "{") {
		r, err := regexp.Compile("{(.*?)}")
		if err != nil {
			return nil, err
		}

		noteIDString := r.FindStringSubmatch(line)[1]
		noteIDInt64, err := strconv.ParseInt(noteIDString, 10, 64)
		if err != nil {
			return nil, err
		}
		if noteIDInt64 != 0 {
			noteID = &noteIDInt64
		}

		// set end of noteID (may or may not exist) to startIndex
		startIndex = r.FindStringSubmatchIndex(line)[1]
	}

	split := strings.SplitN(line[startIndex:], ":", 2)
	switch len(split) {
	case 2:
		return &InternalNote{
			front:      split[0],
			back:       split[1],
			deck:       deckName,
			id:         noteID,
			lineNumber: lineNumber,
		}, nil
	default:
		return nil, errors.New(fmt.Sprintf("malformed line found: %s", line))
	}
}
