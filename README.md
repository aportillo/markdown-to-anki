# markdown-to-anki
Reads markdown files to create Anki decks/cards via Anki Connect API.
https://foosoft.net/projects/anki-connect/

## sample
file: keyboard-shortcuts.md

content:
``` markdown
...
# osx
- hide window: ⌘ + h
- hide all other windows: ⌘ + alt + h
...
```

## explanation:
- `# osx` -> represents the name of deck
- each bullet/newline will be parsed as a card, i.e. "card-front: card-back"
- skips duplicates and creates deck `osx` if it doesn't already exist.


# todo
- support editing in markdown file and syncing updates (edits/deletes) to anki db
- support code blocks
